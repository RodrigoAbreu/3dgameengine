#pragma once
#include "pch.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

class Graphics
{
private:
	std::unique_ptr<GeometricPrimitive> shape3D;

protected:
		ID3D12GraphicsCommandList * graphicsCommandList;
		std::unique_ptr<DirectX::BasicEffect> basicEffectLine;
		std::unique_ptr<DirectX::BasicEffect> basicEffectPrimitive;
		std::unique_ptr<DirectX::PrimitiveBatch<DirectX::VertexPositionColor>> primitiveBatch;

		DirectX::SimpleMath::Matrix m_world;
		DirectX::SimpleMath::Matrix m_view;
		DirectX::SimpleMath::Matrix m_proj;

	public:
		Graphics();
		Graphics(ID3D12GraphicsCommandList * graphicsCommandList);
		~Graphics();

		void initDevice(ID3D12Device * m_d3dDevice);
		void initResources(int m_outputWidth, int m_outputHeight);
		void updateWorldMatrix();
		void resetComponents();

		//2D Shapes
		void drawLine(Vector3 start, Vector3 end, XMVECTORF32 color);
		void drawTriangle(Vector3 top, Vector3 right, Vector3 left, XMVECTORF32 color);
		void drawRectangle(Vector3 upperLeft, Vector3 upperRight, Vector3 lowerLeft, Vector3 lowerRight, XMVECTORF32 color);

		//3D Shapes
		void drawSphere(float diameter, int tessellation);
		void drawTorus(float diameter, float thickness, int tessellation);
		void drawCube(float size);
		void drawCone(float diameter, float height, int tessellation);
		void drawCylinder(float diameter, float height, int tessellation);
		void drawDodecahedron(float size);
		void drawTeapot(float size, int tessellation);
		void drawIcosahedron(float size);
		void drawOctahedron(float size);
		void drawTetrahedron(float size);
};