#include "pch.h"
#include "Graphics.h"

using namespace DirectX;
using namespace DirectX::SimpleMath;

Graphics::Graphics()
{
}

Graphics::Graphics(ID3D12GraphicsCommandList * graphicsCommandList)
	: graphicsCommandList(graphicsCommandList)
{
}

Graphics::~Graphics()
{
}

void Graphics::initDevice(ID3D12Device * m_d3dDevice)
{
	m_world = Matrix::Identity;
	primitiveBatch = std::make_unique<PrimitiveBatch<VertexPositionColor>>(m_d3dDevice);

	RenderTargetState rtState(DXGI_FORMAT_B8G8R8A8_UNORM, DXGI_FORMAT_D32_FLOAT);
	rtState.sampleDesc.Count = 4; // <--- 4x MSAA

	CD3DX12_RASTERIZER_DESC rastDesc(D3D12_FILL_MODE_SOLID, D3D12_CULL_MODE_NONE, FALSE,
		D3D12_DEFAULT_DEPTH_BIAS, D3D12_DEFAULT_DEPTH_BIAS_CLAMP,
		D3D12_DEFAULT_SLOPE_SCALED_DEPTH_BIAS, TRUE, TRUE, FALSE,
		0, D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF);

	EffectPipelineStateDescription pdLine(
		&VertexPositionColor::InputLayout,
		CommonStates::Opaque,
		CommonStates::DepthDefault,
		rastDesc,
		rtState,
		D3D12_PRIMITIVE_TOPOLOGY_TYPE_LINE);

	basicEffectLine = std::make_unique<BasicEffect>(m_d3dDevice, EffectFlags::VertexColor, pdLine);

	EffectPipelineStateDescription pdPrimitive(
		&VertexPositionColor::InputLayout,
		CommonStates::Opaque,
		CommonStates::DepthDefault,
		CommonStates::CullNone,
		rtState);

	basicEffectPrimitive = std::make_unique<BasicEffect>(m_d3dDevice, EffectFlags::VertexColor, pdPrimitive);
}

void Graphics::initResources(int m_outputWidth, int m_outputHeight)
{
	UINT backBufferWidth = static_cast<UINT>(m_outputWidth);
	UINT backBufferHeight = static_cast<UINT>(m_outputHeight);

	m_view = Matrix::CreateLookAt(Vector3(0.f, 0.f, 2.f), Vector3::Zero, Vector3::UnitY);
	m_proj = Matrix::CreatePerspectiveFieldOfView(XM_PI / 2.f, float(backBufferWidth) / float(backBufferHeight), 0.1f, 10.f);

	basicEffectLine->SetView(m_view);
	basicEffectPrimitive->SetView(m_view);

	basicEffectLine->SetProjection(m_proj);
	basicEffectPrimitive->SetProjection(m_proj);
}

void Graphics::updateWorldMatrix()
{
	basicEffectLine->SetWorld(m_world);
	basicEffectPrimitive->SetWorld(m_world);
}

void Graphics::resetComponents()
{
	basicEffectLine.reset();
	basicEffectPrimitive.reset();
	primitiveBatch.reset();
	shape3D.reset();
}

void Graphics::drawLine(Vector3 start, Vector3 end, XMVECTORF32 color)
{
	basicEffectLine->Apply(graphicsCommandList);
	primitiveBatch->Begin(graphicsCommandList);

	VertexPositionColor v1(start, color);
	VertexPositionColor v2(end, color);
	primitiveBatch->DrawLine(v1, v2);

	primitiveBatch->End();
}

void Graphics::drawTriangle(Vector3 top, Vector3 right, Vector3 left, XMVECTORF32 color)
{
	basicEffectPrimitive->Apply(graphicsCommandList);
	primitiveBatch->Begin(graphicsCommandList);

	VertexPositionColor v_top(top, color);
	VertexPositionColor v_right(right, color);
	VertexPositionColor v_left(left, color);
	primitiveBatch->DrawTriangle(v_top, v_right, v_left);
	
	primitiveBatch->End();
}

void Graphics::drawRectangle(Vector3 upperLeft, Vector3 upperRight, Vector3 lowerLeft, Vector3 lowerRight, XMVECTORF32 color)
{
	basicEffectPrimitive->Apply(graphicsCommandList);
	primitiveBatch->Begin(graphicsCommandList);
	
	VertexPositionColor v_topLeft(upperLeft, color);
	VertexPositionColor v_topRight(upperRight, color);
	VertexPositionColor v_bottomRight(lowerLeft, color);
	VertexPositionColor v_bottomLeft(lowerRight, color);
	primitiveBatch->DrawQuad(v_topLeft, v_topRight, v_bottomRight, v_bottomLeft);
	
	primitiveBatch->End();
}

void Graphics::drawSphere(float diameter, int tessellation)
{
	shape3D = GeometricPrimitive::CreateSphere(diameter, tessellation, true, false);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawTorus(float diameter, float thickness, int tessellation)
{
	shape3D = GeometricPrimitive::CreateTorus(diameter, thickness, tessellation, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawCube(float size)
{
	shape3D = GeometricPrimitive::CreateCube(size, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawCone(float diameter, float height, int tessellation)
{
	shape3D = GeometricPrimitive::CreateCone(diameter, height, tessellation, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawCylinder(float diameter, float height, int tessellation)
{
	shape3D = GeometricPrimitive::CreateCylinder(diameter, height, tessellation, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawDodecahedron(float size)
{
	shape3D = GeometricPrimitive::CreateDodecahedron(size, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawTeapot(float size, int tessellation)
{
	shape3D = GeometricPrimitive::CreateTeapot(size, tessellation, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawIcosahedron(float size)
{
	shape3D = GeometricPrimitive::CreateIcosahedron(size, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawOctahedron(float size)
{
	shape3D = GeometricPrimitive::CreateOctahedron(size, true);
	shape3D->Draw(graphicsCommandList);
}

void Graphics::drawTetrahedron(float size)
{
	shape3D = GeometricPrimitive::CreateTetrahedron(size, true);
	shape3D->Draw(graphicsCommandList);
}