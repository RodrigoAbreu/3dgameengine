#include "pch.h"
#include "GameVector4.h"


float& GameVector4::operator[](const unsigned int index)
{
	switch (index)
	{
	case 0:
		return x;
		break;
	case 1:
		return y;
		break;
	case 2:
		return z;
		break;
	case 3:
		return w;
		break;
	default:
		//TODO:: Implement exception handler
		float error = 0;
		return error;
		break;
	}

}

GameVector4::GameVector4()
{
	x = 0;
	y = 0;
	z = 0;
	w = 0;
}

GameVector4::GameVector4(float valueX, float valueY, float valueZ, float valueW)
{
	x = valueX;
	y = valueY;
	z = valueZ;
	w = valueW;
}


GameVector4::~GameVector4()
{
}
