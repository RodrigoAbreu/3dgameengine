#pragma once
class GameVector4
{
public:
	float x, y, z, w;
	float& operator[](const unsigned int index);
	GameVector4(float valueX, float valueY, float valueZ, float valueW);
	GameVector4();
	~GameVector4();
};

