#pragma once
#include "CustomMatrix.h"

#define PI 3.1415f

enum Axis { X, Y, Z, W };
enum Dimensions { ONE_DIMENSION = 1, TWO_DIMENSIONS = 2, THREE_DIMENSIONS = 3, FOUR_DIMENSIONS = 4 };
enum CustomLine { START_POINT, END_POINT };
enum CustomRectangle { UPPER_LEFT, UPPER_RIGHT, LOWER_RIGHT, LOWER_LEFT };
enum CustomTriangle { TOP, LEFT, RIGHT };
enum CustomCircle { CENTER };
enum Transformations { TRANSLATION, ROTATION, SCALE, REFLECTION };

class MathUtils
{
	public:
		static float degToRad(float degrees);
		static float radToDeg(float radians);
		static float sinDegrees(float angle);
		static float cosDegrees(float angle);
		static float sinRadians(float radians);
		static float cosRadians(float radians);
};

class MatrixUtils {
	public:
		static CustomMatrix getIdentityMatrix(size_t matrixSize);
		static CustomMatrix getTranslationMatrix(float translationX, float translationY, float translationZ);
		static CustomMatrix getScaleMatrix(float scaleX, float scaleY, float scaleZ);
		static CustomMatrix getReflectionMatrix(bool reflectionX, bool reflectionY, bool reflectionZ);
		static CustomMatrix getRotationMatrix(float degrees, Axis axis);
};