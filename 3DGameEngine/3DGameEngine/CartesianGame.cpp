#include "pch.h"
#include "CartesianGame.h"

CartesianGame::CartesianGame()
{
}

CartesianGame::~CartesianGame() 
{

}

void CartesianGame::initDevice()
{	
}

void CartesianGame::initResources()
{
	components = std::make_unique<vector<GameComponent *>>();

	// GRID
	LineShape * line;
	Vector3 xaxis(2.f, 0.f, 0.f);
	Vector3 yaxis(0.f, 0.f, 2.f);
	Vector3 origin = Vector3::Zero;

	size_t divisions = 15;

	for (size_t i = 0; i <= divisions; ++i)
	{
		line = new LineShape();

		float fPercent = float(i) / float(divisions);
		fPercent = (fPercent * 2.0f) - 1.0f;
		Vector3 scale = xaxis * fPercent + origin;

		Vector3 start = scale - yaxis;
		Vector3 end = scale + yaxis;

		line->setVector(CustomLine::START_POINT, { start.x, start.y, start.z, 1.f });
		line->setVector(CustomLine::END_POINT, { end.x, end.y, end.z, 1.f });
		line->setColor(Colors::White);

		components->push_back(line);
	}

	for (size_t i = 0; i <= divisions; i++)
	{
		line = new LineShape();

		float fPercent = float(i) / float(divisions);
		fPercent = (fPercent * 2.0f) - 1.0f;
		Vector3 scale = yaxis * fPercent + origin;

		Vector3 start = scale - xaxis;
		Vector3 end = scale + xaxis;

		line->setVector(CustomLine::START_POINT, { start.x, start.y, start.z, 1.f });
		line->setVector(CustomLine::END_POINT, { end.x, end.y, end.z, 1.f });
		line->setColor(Colors::White);

		components->push_back(line);
	}
	// Axis X
	line = new LineShape();

	line->setVector(CustomLine::START_POINT, { -2.f, 0.f, 0.f, 1.f });
	line->setVector(CustomLine::END_POINT, { 2.f, 0.f, 0.f, 1.f });
	line->setColor(Colors::Black);

	components->push_back(line);

	// Axis Y
	line = new LineShape();
	line->setVector(CustomLine::START_POINT, { 0.f, -2.f, 0.f, 1.f });
	line->setVector(CustomLine::END_POINT, { 0.f, 2.f, 0.f, 1.f });
	line->setColor(Colors::Black);

	components->push_back(line);
	
	// Rectangle
	RectangleShape * rectangle = new RectangleShape();
	rectangle->setVector(CustomRectangle::UPPER_LEFT, { -0.9f, 1.8f, 0.f, 1.f });
	rectangle->setVector(CustomRectangle::UPPER_RIGHT, { -0.2f, 1.8f, 0.f, 1.f });
	rectangle->setVector(CustomRectangle::LOWER_RIGHT, { -0.2f, 1.1f, 0.f, 1.f });
	rectangle->setVector(CustomRectangle::LOWER_LEFT, { -0.9f, 1.1f, 0.f, 1.f });
	components->push_back(rectangle);

	RectangleShape * reflectionRectangle = new RectangleShape(*rectangle);
	reflectionRectangle->reflect(true, false, false, true);
	components->push_back(reflectionRectangle);

	RectangleShape * scaleRectangle = new RectangleShape(*rectangle);
	scaleRectangle->translate(0.f, -2.f, 0.f, true);
	scaleRectangle->scale(0.5f, 0.5f, 1.f, false);
	components->push_back(scaleRectangle);

	RectangleShape * rotationRectangle = new RectangleShape(*rectangle);
	rotationRectangle->translate(2.f, -2.f, 0.f, true);
	rotationRectangle->rotate(-45.f, Axis::Z, false);
	components->push_back(rotationRectangle);
}

void CartesianGame::updateComponents(DX::StepTimer const& timer)
{
	//m_world = Matrix::CreateRotationY(cosf(static_cast<float>(timer.GetTotalSeconds())));
}

void CartesianGame::renderComponents()
{
	graphics->updateWorldMatrix();

	for (size_t i = 0; i < components->size(); i++) {
		components->at(i)->draw(graphics.get());
	}
}

void CartesianGame::resetComponents()
{
	
}
