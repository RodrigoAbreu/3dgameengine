#include "pch.h"
#include "Utils.h"

float MathUtils::degToRad(float degrees) {
	// R = D*PI / 180;
	return degrees * PI / 180.0f;
}

float MathUtils::radToDeg(float radians) {
	// 180R/PI = D
	return 180.0f * radians / PI;
}

float MathUtils::sinDegrees(float angle) {
	return sin(degToRad(angle));
}

float MathUtils::cosDegrees(float angle) {
	return cos(degToRad(angle));
}

float MathUtils::sinRadians(float radians) {
	return sin(radians);
}

float MathUtils::cosRadians(float radians) {
	return cos(radians);
}

CustomMatrix MatrixUtils::getIdentityMatrix(size_t matrixSize) {
	CustomMatrix result(matrixSize, matrixSize);

	for (size_t index = 0; index < matrixSize; index++) {
		result[index][index] = 1.f;
	}

	return result;
}

CustomMatrix MatrixUtils::getTranslationMatrix(float translationX, float translationY, float translationZ) {
	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);

	for (size_t index = 0; index < Dimensions::FOUR_DIMENSIONS; index++) {
		result[index][index] = 1.f;
	}

	result[Axis::W][Axis::X] = translationX;
	result[Axis::W][Axis::Y] = translationY;
	result[Axis::W][Axis::Z] = translationZ;

	return result;
}

CustomMatrix MatrixUtils::getScaleMatrix(float scaleX, float scaleY, float scaleZ) {
	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);

	result[Axis::X][Axis::X] = scaleX;
	result[Axis::Y][Axis::Y] = scaleY;
	result[Axis::Z][Axis::Z] = scaleZ;
	result[Axis::W][Axis::W] = 1.f;

	return result;
}

CustomMatrix MatrixUtils::getReflectionMatrix(bool reflectionX, bool reflectionY, bool reflectionZ) {
	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);

	result[Axis::X][Axis::X] = reflectionX ? -1.f : 1.f;
	result[Axis::Y][Axis::Y] = reflectionY ? -1.f : 1.f;
	result[Axis::Z][Axis::Z] = reflectionZ ? -1.f : 1.f;
	result[Axis::W][Axis::W] = 1.f;

	return result;
}

CustomMatrix MatrixUtils::getRotationMatrix(float degrees, Axis axis) {
	CustomMatrix result(Dimensions::FOUR_DIMENSIONS, Dimensions::FOUR_DIMENSIONS);
	float radians = MathUtils::degToRad(degrees);

	result[0][Axis::X] = (Axis::X == axis ? 1.f : MathUtils::cosRadians(radians));
	result[0][Axis::Y] = (Axis::Z == axis ? MathUtils::sinRadians(radians) : 0.f);
	result[0][Axis::Z] = (Axis::Y == axis ? -1 * MathUtils::sinRadians(radians) : 0.f);

	result[1][Axis::X] = (Axis::Z == axis ? -1 * MathUtils::sinRadians(radians) : 0.f);
	result[1][Axis::Y] = (Axis::Y == axis ? 1.f : MathUtils::cosRadians(radians));
	result[1][Axis::Z] = (Axis::X == axis ? MathUtils::sinRadians(radians) : 0.f);

	result[2][Axis::X] = (Axis::Y == axis ? MathUtils::sinRadians(radians) : 0.f);
	result[2][Axis::Y] = (Axis::X == axis ? -1 * MathUtils::sinRadians(radians) : 0.f);
	result[2][Axis::Z] = (Axis::Z == axis ? 1.f : MathUtils::cosRadians(radians));

	result[3][Axis::W] = 1.f;

	return result;
}