#pragma once
#include <stdexcept>
class GameVector3
{
public:
	float x, y, z;
	float& operator[](const unsigned int index);
	GameVector3();
	GameVector3(float valueX,float valueY,float valueZ);
	~GameVector3();
};

