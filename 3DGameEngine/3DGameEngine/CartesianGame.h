#pragma once
#include <vector>
#include "Game.h"
#include "GameComponent.h"

using std::vector;

class CartesianGame : public Game
{
	public:
		CartesianGame();
		~CartesianGame();

	protected:
		std::unique_ptr<vector<GameComponent *>> components;

		void initDevice() override;
		void initResources() override;
		void updateComponents(DX::StepTimer const& timer) override;
		void renderComponents() override;
		void resetComponents() override;
};